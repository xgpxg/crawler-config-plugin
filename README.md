# crawler-config-plugin

![输入图片说明](https://images.gitee.com/uploads/images/2019/0628/192919_c7bb0d1b_1537128.png "5LYD5G`K_J}C]8(YX(Q@801.png")

#### 介绍
基于vue的爬虫采集字段可视化点选配置chrome插件

#### 使用方式
下载工程：
git clone https://gitee.com/xgpxg/crawler-config-plugin.git

将plugin目录作为已解压插件添加到chrome浏览器

ctr+shift+z 打开/关闭插件界面

点击网页上你想采集的元素即可。

当配置完一个字段组后，点击“暂存字段组”按钮，即可完成一个字段组的配置。配置完所有字段后，点击“复制”按钮即可将json数据复制到剪贴板。

样例json：


```
{
    "group_0": [
        {
            "fieldCode": "title", 
            "fieldName": "标题", 
            "fieldRule": "div#1 > h3:nth-child(1) > a", 
            "sampleValue": "鸡你太美是什么梗?为什么我同学经常开玩笑_百度知道", 
            "_heyui_uuid": "e264db9d-54ba-d856-54df-ae0d511cb113"
        }, 
        {
            "fieldCode": "summary", 
            "fieldName": "简介", 
            "fieldRule": "div.c-abstract.bg-green-color", 
            "sampleValue": "最佳答案: 你们凭什么发这种东西骂人,这叫网络暴力,很容易出人命的,比如我快笑死了😢", 
            "_heyui_uuid": "486aa727-2b60-967c-6ab3-3a32c17ea4b0"
        }, 
        {
            "fieldCode": "from", 
            "fieldName": "来源", 
            "fieldRule": "div#1 > div:nth-child(4) > a:nth-child(1) > span:nth-child(2)", 
            "sampleValue": "百度知道", 
            "_heyui_uuid": "ab26ac8a-577c-366a-832c-213bc30ede30"
        }
    ], 
    "group_1": [
        {
            "fieldCode": "title", 
            "fieldName": "标题", 
            "fieldRule": "div#2 > h3:nth-child(3) > a", 
            "sampleValue": "【蔡徐坤】 鸡你太美!-娱乐-高清正版视频在线观看–爱奇艺", 
            "_heyui_uuid": "2ffb975d-5c2b-3da8-af6c-5bcb41d54571"
        }, 
        {
            "fieldCode": "summary", 
            "fieldName": "简介", 
            "fieldRule": "div#2 > div:nth-child(4) > div:nth-child(2) > font > p:nth-child(2)", 
            "sampleValue": "2019年3月18日 - 【蔡徐坤】鸡你太美！是娱乐类高清视频，画面清晰，播放流畅，发布时间：20190318。节目简介：", 
            "_heyui_uuid": "b7d85413-2039-5e9f-c311-5dac7bb50f35"
        }, 
        {
            "fieldCode": "from", 
            "fieldName": "来源", 
            "fieldRule": "div#2 > div:nth-child(4) > div:nth-child(2) > font > div:nth-child(3) > a:nth-child(2)", 
            "sampleValue": "百度快照", 
            "_heyui_uuid": "39c1d4a8-9e63-0bca-4be8-74e2fd30ff74"
        }
    ]
}

```

