function init() {
  var app = new Vue({
    el: "#crawlerChromeApp",
    data: {
      message: "Hello Vue!",
      show: false,
      border: true,
      stripe: true,
      checkbox: false,
      serial: false,
      loading: false,
      datas: [],
      finalData:{}
    },
    methods: {
      openModal: function () {
        this.$Modal({
          title: "title",
          content: "This is a js call box"
        });
      },
      cleanDatas(){
        for(let index in this.datas){
          let rule = this.datas[index].fieldRule;
          $(rule).removeClass('bg-green-color');
        }
        this.datas = [];
      },
      remove(datas,data){
        datas.splice(datas.indexOf(data), 1);
        $(data.fieldRule).removeClass('bg-green-color');
      },
      copy(){
        this.$Clipboard({
          text: JSON.stringify(this.finalData)
        });
      },
      saveGroup(){
        let len = 0;
        for(let i in this.finalData) {
          len++;
        }
        let group = 'group_'+len;
        this.finalData[group] = this.datas;
        this.datas = [];
        this.$Message("保存成功");
      },
      deleteGroup(key){
        let res = {};
        let index = 0;
        for(let k in this.finalData){
          if(k===key)
            continue;
          res['group_'+index] = this.finalData[k];
          index++;
        }
        this.finalData = res;
        this.$Message("删除成功");
      },
      getCssPath(callback) {
        let all = $('*');
        let that = this;
        all.click(function () {
          let path = that.cssPath($(this));
          if($(path).parents('#crawlerChromeApp').length!==0){
            return false;
          }
          if ($(path).hasClass('bg-green-color')) {
            //$(path).removeClass('bg-green-color');
          } else {
            $(path).addClass('bg-green-color');
          }
          if (callback)
            callback(path, $(this));
          return false;
        })
      },
      cssPath(el) {
        let path = [];
        while (el && el[0].tagName !== 'HTML') {
          let selector = el[0].tagName.toLowerCase();
          if (el.attr('id')) {
            selector += '#' + el.attr('id');
            path.unshift(selector);
            break;
          } else {
            if (el.attr('class') && el.attr('class') !== '' && el.attr('class').trim() !== 'bg-green-color' && selector !== 'input') {
              let classStr = el.attr('class');
              let cl = '.' + classStr.trim().split(/\s+/).join('.');
              if ($(selector + cl + '>' + (path.length > 0 ? path.join(" > ") : '')).length === 1) {
                path.unshift(selector + cl);
                break;
              }
              let suffixLen = el.nextAll();
              let nth = el.prevAll().length + 1;
              if (nth !== 1 || suffixLen !== 0)
                selector += ":nth-child(" + nth + ")";
            } else if (selector === 'input' && el.attr('name')) {
              if ($(selector + '[name=' + el.attr('name') + ']').length === 1) {
                path.unshift(selector + '[name=' + el.attr('name') + ']');
                break;
              }
            } else {
              let nth = el.prevAll().length + 1;
              if (nth !== 1)
                selector += ":nth-child(" + nth + ")";
            }
          }
          path.unshift(selector);
          el = el.parent();
        }
        return path.join(" > ");
      }
    },

    mounted: function () {
      let that = this;
      setTimeout(() => {
        that.getCssPath((path) => {
          if($(path).parents('#crawlerChromeApp').length===0){
            let exits = false;
            for(let index in that.datas){
              let data = that.datas[index];
              if(data.fieldRule===path){
                exits = true;
              }
            }
            if(!exits)
              that.datas.push({fieldType: '1000', fieldName: '字段名称', fieldRule: path, sampleValue: $(path).text()});
          }
        })
      }, 1000);
    }
  });
}
function isParent(obj, parentObj) {
  console.log($(obj).html())
  while (obj !== undefined && obj !== null&&  obj[0].tagName.toUpperCase() !=='BODY')
  {
    if (obj === parentObj) {
      return true;
    }
    obj = obj.parent();
  }
  return false;
}
let isOpen = false;
$(document).ready(function () {
  document.onkeydown = function (e) {
    if (e.ctrlKey && e.shiftKey && e.keyCode === 90 && !isOpen) {
      let html =
        '<div id="crawlerChromeApp" style="width: 600px;height: 90%;position: fixed;z-index:100000;top: 0;right: 10px;font-size: 0.7em;">\n' +
        ' <div class="h-panel ">\n' +
        '      <div class="h-panel-bar" style="font-size: 14px">\n' +
        '        <span class="h-panel-title" >采集字段点选工具</span>\n' +
        '      </div>\n' +
        '      <div class="h-panel-body">\n' +
        '         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/heyui/themes/index.css"/>' +
        '           <h-table :height="400" :datas="datas" :border="true" :checkbox="checkbox" :stripe="stripe" :loading="loading" style="font-size: 15px">\n' +
        '             <h-table-item title="序号"  :width="50"  prop="$serial" v-if="true"></h-table-item>\n' +
        '             <h-table-item title="字段名称" :width="100">' +
        '               <template slot-scope="{data}">\n' +
        '                <input style="width: 80px;border: none"  type="text" v-model="data.fieldName"/>'+
        '               </template>' +
        '             </h-table-item>\n' +
        '             <h-table-item title="字段规则" :width="220" prop="fieldRule"></h-table-item>\n' +
        '             <h-table-item title="样例值" :width="220" prop="sampleValue"></h-table-item>\n' +
        '             <h-table-item title="操作" :width="220">' +
        '               <template slot-scope="{data}">\n' +
        '                <button class="h-btn h-btn-xs h-btn-red" @click="remove(datas,data)">删除</button>\n' +
        '               </template>' +
        '             </h-table-item>\n' +
        '             <div slot="empty">暂时无数据</div>\n' +
        '         </h-table>' +
        '       <button class="h-btn" @click="cleanDatas">清空</button>'+
        '       <button class="h-btn h-btn-blue" @click="saveGroup">暂存字段组</button>'+
        '       <button class="h-btn h-btn-green" @click="copy" >复制规则</button>'+
        '       </div><div class="bottom-line"></div>' +
        '      <div class="h-panel-bar" style="font-size: 14px">\n' +
        '       <span>已添加的字段组(双击可删除)：</span><span class="h-tag" v-for="(value,key,index) in finalData" @dblclick="deleteGroup(key)">字段组{{index}}</span>'+
        '      </div>\n' +
        '      </div>' +
        '  </div>';
      $('html').eq(0).append(html);
      init();
      isOpen = true;
    }
    else if (e.ctrlKey && e.shiftKey && e.keyCode === 90 && isOpen) {
      $('#crawlerChromeApp').remove();
      isOpen = false;
    }
  }
});

